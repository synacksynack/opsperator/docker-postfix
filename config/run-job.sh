#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

WAIT_INTERVAL=${WAIT_INTERVAL:-3600}
if ! test "$WAIT_INTERVAL" -ge 300 >/dev/null 2>&1; then
    WAIT_INTERVAL=3600
fi

while :
do
    ds=`date +%s`
    echo "Starting Postfix Maps Refresh on $(date)"
    . /usr/local/bin/makemaps.sh
    de=`date +%s`
    elapsed=`expr $ds - $ds`
    w=`expr $WAIT_INTERVAL - $elapsed`
    if test $w -lt 300; then
	w=300
    fi
    cat <<EOF
Done on `date` - ran $elapsed seconds
Waiting $w seconds until next refresh
EOF
    sleep $w
done
