#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

DKIM_HOST=${DKIM_HOST:-127.0.0.1}
DKIM_PORT=${DKIM_PORT:-12345}
if ls /etc/dkim.d/keys/*/default.private >/dev/null 2>&1; then
    if ! test -s /etc/opendkim.conf; then
	cat <<EOF >/etc/opendkim.conf
Syslog yes
UMask 002
OversignHeaders From
KeyTable /etc/dkim.d/KeyTable
SigningTable /etc/dkim.d/SigningTable
ExternalIgnoreList /etc/dkim.d/TrustedHosts
InternalHosts /etc/dkim.d/TrustedHosts
PidFile /run/opendkim/opendkim.pid
Socket inet:$DKIM_PORT@$DKIM_HOST
EOF
    fi
    if ! test -s /etc/dkim.d/KeyTable; then
	ls /etc/dkim.d/keys/*/*.private 2>/dev/null | while read f
	do
	    d=`echo $f | cut -d/ -f5`
	    k=`echo $f | sed 's|.*/\([a-zA-Z0-9]*\)\.private|\1|'`
	    echo $k._domainkey.$d $d:$k:$f
	done >/etc/dkim.d/KeyTable
    fi
    if ! test -s /etc/dkim.d/SigningTable; then
	ls /etc/dkim.d/keys/*/*.private 2>/dev/null | while read f
	do
	    d=`echo $f | cut -d/ -f5`
	    k=`echo $f | sed 's|.*/\([a-zA-Z0-9]*\)\.private|\1|'`
	    echo $d $k._domainkey.$d
	done >/etc/dkim.d/SigningTable
    fi
    if ! test -s /etc/dkim.d/TrustedHosts; then
	if test -z "$MYNETWORKS"; then
	    MYNETWORKS=127.0.0.1/32
	fi
	for net in $MYNETWORKS
	do
	    echo $net
	done >/etc/dkim.d/TrustedHosts
	if ! grep localhost /etc/dkim.d/TrustedHosts >/dev/null; then
	    echo localhost >>/etc/dkim.d/TrustedHosts
	fi
    fi
fi

while :
do
    if test -e /run/rsyslog/dev/log; then
	echo rsyslogd ready
	break
    fi
    echo waiting for rsyslogd ...
    sleep 10
done

unset MYNETWORKS

mkdir -p /run/opendkim
chown opendkim:opendkim /run/opendkim

exec /usr/sbin/opendkim -f -P /run/opendkim/opendkim.pid -p inet:$DKIM_PORT@$DKIM_HOST
