#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

. /usr/local/bin/reset-tls.sh
if ! test -e /etc/saslauthd.conf; then
    if ! ls /etc/saslauthd.conf.d/* >/dev/null 2>&1; then
	OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=postfix,ou=services}"
	OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
	OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
	OPENLDAP_HOST=${OPENLDAP_HOST:-}
	OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
	OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
	if test -z "$OPENLDAP_BASE"; then
	    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
	fi
	if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
	    OPENLDAP_PORT=636
	elif test -z "$OPENLDAP_PORT"; then
	    OPENLDAP_PORT=389
	fi
	cat <<EOF >/etc/saslauthd.conf
auxprop_plugin: ldapdb
ldap_auth_method: bind
ldap_bind_dn: $OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE
ldap_bind_pw: $OPENLDAP_BIND_PW
ldap_default_domain: $OPENLDAP_DOMAIN
ldap_filter: (&(objectClass=$OPENLDAP_USERS_OBJECTCLASS)(|(uid=%u)(mail=%u)(mail=%u@$OPENLDAP_DOMAIN)))
ldap_search_base: ou=users,$OPENLDAP_BASE
ldap_servers: $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT
ldap_scope: sub
ldap_version: 3
EOF
	if test -s /certs/ca.crt; then
	    cat <<EOF >/etc/saslauthd.conf
ldap_tls_check_peer: yes
ldap_tls_cacert_file: /certs/ca.crt
EOF
	elif test -f /run/secrets/kubernetes.io/serviceaccount/ca.crt; then
	    cat <<EOF >>/etc/saslauthd.conf
ldap_tls_check_peer: yes
ldap_tls_cacert_file: /run/secrets/kubernetes.io/serviceaccount/ca.crt
EOF
	else
	    cat <<EOF >>/etc/saslauthd.conf
ldap_tls_check_peer: yes
ldap_tls_cacert_dir: /etc/ssl/certs/
EOF
	fi
    else
	cat /etc/saslauthd.conf.d/*.conf >>/etc/saslauthd.conf
    fi
fi

while :
do
    if test -e /run/rsyslog/dev/log; then
	echo rsyslogd ready
	break
    fi
    echo waiting for rsyslogd ...
    sleep 10
done

unset OPENLDAP_BIND_DN_PREFIX OPENLDAP_BIND_PW OPENLDAP_DOMAIN \
    OPENLDAP_HOST OPENLDAP_PROTO OPENLDAP_USERS_OBJECTCLASS

exec /usr/sbin/saslauthd -O /etc/saslauthd.conf -c -V -d -m /run/saslauthd -a ldap
