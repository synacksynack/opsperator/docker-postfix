#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

LMTP_HOST=${LMTP_HOST:-127.0.0.1}
LMTP_PORT=${LMTP_PORT:-2400}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=postfix,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi

NEW_TRANSPORTS=/tmp/transport
NEW_VIRTUAL_ALIASES=/tmp/virtual_alias
NEW_VIRTUAL_DOMAINS=/tmp/virtual_domains
NEW_VIRTUAL_MAILBOXES=/tmp/virtual_mailbox
>$NEW_TRANSPORTS
>$NEW_VIRTUAL_ALIASES
>$NEW_VIRTUAL_DOMAINS
>$NEW_VIRTUAL_MAILBOXES

check_domain()
{
    local d
    if test -z "$1"; then
	return 0
    fi
    d=`echo "$1" | cut -d@ -f2`
    if ! grep -E "^$d OK" $NEW_VIRTUAL_DOMAINS >/dev/null; then
	echo $d OK >>$NEW_VIRTUAL_DOMAINS
    fi
}

check_address_valid()
{
    local address
    if test -z "$1"; then
	return 0
    fi
    address="$1"
    d=`echo "$address" | cut -d@ -f2 | tr [A-Z] [a-z]`
    u=`echo "$address" | cut -d@ -f1 | tr [A-Z] [a-z]`
    if ! echo "$address" | tr [A-Z] [a-z] | grep -E \
	    '^[a-z0-9][a-z0-9_\.-]*@[a-z0-9][a-z0-9_\.-]*\.[a-z0-9]*$' \
	    >/dev/null; then
	echo "FATAL: invalid address $address - does not match email regexpr"
	return 1
    elif test `echo "$address" | wc -c` -gt 321 >/dev/null 2>&1; then
	echo "FATAL: invalid address $address - too long"
	return 1
    elif test `echo "$d" | wc -c` -gt 256 >/dev/null 2>&1; then
	echo FATAL: invalid address $address - domain part too long
	return 1
    elif test `echo "$u" | wc -c` -gt 65 >/dev/null 2>&1; then
	echo FATAL: invalid address $address - user part too long
	return 1
    elif ! echo "$u" | grep -E '^[a-z][a-z0-9_\.-]*$' >/dev/null; then
	echo FATAL: username has invalid characters
	return 1
    elif test `echo "$u" | wc -c` -gt 33 >/dev/null 2>&1; then
	echo FATAL: username too long
	return 1
    fi
    return 0
}

# Fetches User Mailbox Addresses
# Fetches User Aliases

echo === Querying LDAP for User Mailboxes and their Aliases ===
echo ===

ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
	    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
	    -b "ou=users,$OPENLDAP_BASE" \
	    -w "$OPENLDAP_BIND_PW" \
	    "(mail=*)" uid 2>&1 \
    | awk '/^uid:/{print $2}' \
    | while read uid
    do
	mainAddress=$(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
				    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
				    -b "ou=users,$OPENLDAP_BASE" \
				    -w "$OPENLDAP_BIND_PW" \
				    "(uid=$uid)" mail 2>&1 \
			| awk 'BEG{take=0}{if ($1 == "mail:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
			| tr '\n' '|' \
			| sed -e 's,| ,,g' -e 's,|$,,' \
			| tr [A-Z] [a-z])
	if ! check_address_valid $mainAddress; then
	    continue
	fi
	check_domain $mainAddress
	echo "$mainAddress OK" >>$NEW_VIRTUAL_MAILBOXES
	echo "$mainAddress $mainAddress" >>$NEW_VIRTUAL_ALIASES
	echo "$mainAddress lmtp:$LMTP_HOST:$LMTP_PORT" >>$NEW_TRANSPORTS
	for aliasAddress in $(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
					    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
					    -b "ou=users,$OPENLDAP_BASE" \
					    -w "$OPENLDAP_BIND_PW" \
					    "(uid=$uid)" gosaMailAlternateAddress 2>&1 \
				| awk 'BEG{take=0}{if ($1 == "gosaMailAlternateAddress:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
				| tr '\n' '|' \
				| sed -e 's,| ,,g' -e 's,|$,,' \
				| tr [A-Z] [a-z])
	do
	    if ! check_address_valid $aliasAddress; then
		continue
	    fi
	    check_domain $aliasAddress
	    echo "$aliasAddress $mainAddress" >>$NEW_VIRTUAL_ALIASES
	done
    done

# Fetches Mailing Lists Main Addresses (groupOfNames)
# Fetches Mailing Lists Aliases (groupOfNames)

echo === Querying LDAP for Static Mailing Lists and their Aliases ===
echo ===

ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
	    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
	    -b "ou=groups,$OPENLDAP_BASE" \
	    -w "$OPENLDAP_BIND_PW" \
	    '(&(objectClass=groupOfNames)(mail=*)(objectClass=wsweetGroupMailingList))' cn 2>&1 \
    | awk '/^cn:/{print $2}' \
    | while read cn
    do
	mainAddress=$(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
				    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
				    -b "ou=groups,$OPENLDAP_BASE" \
				    -w "$OPENLDAP_BIND_PW" \
				    "(cn=$cn)" mail 2>&1 \
			| awk 'BEG{take=0}{if ($1 == "mail:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
			| tr '\n' '|' \
			| sed -e 's,| ,,g' -e 's,|$,,' \
			| tr [A-Z] [a-z])
	if ! check_address_valid $mainAddress; then
	    continue
	fi
	check_domain $mainAddress
	membersAddresses=""
	for memberuid in $(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
					-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
					-b "ou=groups,$OPENLDAP_BASE" \
					-w "$OPENLDAP_BIND_PW" \
					"(cn=$cn)" member 2>&1 \
			    | awk '/^member: uid=/{print $2;}' \
			    | sed 's|uid=\([^,]*\),.*|\1|')
	do
	    memberAddress=$(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
					-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
					-b "ou=users,$OPENLDAP_BASE" \
					-w "$OPENLDAP_BIND_PW" \
					"(uid=$memberuid)" mail 2>&1 \
				| awk 'BEG{take=0}{if ($1 == "mail:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
				| tr '\n' '|' \
				| sed -e 's,| ,,g' -e 's,|$,,' \
				| tr [A-Z] [a-z])
	    if ! check_address_valid $memberAddress; then
		continue
	    fi
	    if test "$membersAddresses"; then
		membersAddresses="$membersAddresses,$memberAddress"
	    else
		membersAddresses="$memberAddress"
	    fi
	done
	for aliasAddress in $(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
					    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
					    -b "ou=groups,$OPENLDAP_BASE" \
					    -w "$OPENLDAP_BIND_PW" \
					    "(cn=$cn)" gosaMailAlternateAddress 2>&1 \
				| awk 'BEG{take=0}{if ($1 == "gosaMailAlternateAddress:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
				| tr '\n' '|' \
				| sed -e 's,| ,,g' -e 's,|$,,' \
				| tr [A-Z] [a-z])
	do
	    if ! check_address_valid $aliasAddress; then
		continue
	    fi
	    check_domain $aliasAddress
	    echo "$aliasAddress $mainAddress" >>$NEW_VIRTUAL_ALIASES
	done
	echo "$mainAddress $membersAddresses" >>$NEW_VIRTUAL_ALIASES
    done

# Fetches Mailing Lists Main Addresses (groupOfURLs)
# Fetches Mailing Lists Aliases (groupOfURLs)

echo === Querying LDAP for Dynamic Mailing Lists and their Aliases ===
echo ===

ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
	    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
	    -b "ou=groups,$OPENLDAP_BASE" \
	    -w "$OPENLDAP_BIND_PW" \
	    '(&(objectClass=groupOfURLs)(mail=*)(objectClass=wsweetGroupMailingList))' cn 2>&1 \
    | awk '/^cn:/{print $2}' \
    | while read cn
    do
	mainAddress=$(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
				    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
				    -b "ou=groups,$OPENLDAP_BASE" \
				    -w "$OPENLDAP_BIND_PW" \
				    "(cn=$cn)" mail 2>&1 \
			| awk 'BEG{take=0}{if ($1 == "mail:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
			| tr '\n' '|' \
			| sed -e 's,| ,,g' -e 's,|$,,' \
			| tr [A-Z] [a-z])
	if ! check_address_valid $mainAddress; then
	    continue
	fi
	check_domain $mainAddress
	membersAddresses=""
	memberURL=$(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
				    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
				    -b "ou=groups,$OPENLDAP_BASE" \
				    -w "$OPENLDAP_BIND_PW" \
				    "(cn=$cn)" memberURL 2>&1 \
			| awk 'BEG{take=0}{if ($1 == "memberURL:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
			| tr '\n' '|' \
			| sed -e 's,| ,,g' -e 's,|$,,')
	searchBase=$(echo "$memberURL" | sed -e 's|^ldap[s]*://[0-9\.]*/\([^?]*\)?.*|\1|')
	searchFilter=$(echo "$memberURL" | sed -e "s|.*$searchBase[^(]*\(.*\)|\1|")
	for memberAddress in $(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
					    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
					    -b "$searchBase" \
					    -w "$OPENLDAP_BIND_PW" \
					    "$searchFilter" mail 2>&1 \
				| awk 'BEG{take=0}{if ($1 == "mail:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
				| tr '\n' '|' \
				| sed -e 's,| ,,g' -e 's,|$,,' \
				| tr [A-Z] [a-z])
	do
	    if ! check_address_valid $memberAddress; then
		continue
	    fi
	    if test "$membersAddresses"; then
		membersAddresses="$membersAddresses,$memberAddress"
	    else
		membersAddresses="$memberAddress"
	    fi
	done
	for aliasAddress in $(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
					    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
					    -b "ou=groups,$OPENLDAP_BASE" \
					    -w "$OPENLDAP_BIND_PW" \
					    "(cn=$cn)" gosaMailAlternateAddress 2>&1 \
				| awk 'BEG{take=0}{if ($1 == "gosaMailAlternateAddress:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
				| tr '\n' '|' \
				| sed -e 's,| ,,g' -e 's,|$,,' \
				| tr [A-Z] [a-z])
	do
	    if ! check_address_valid $aliasAddress; then
		continue
	    fi
	    check_domain $aliasAddress
	    echo "$aliasAddress $mainAddress" >>$NEW_VIRTUAL_ALIASES
	done
	echo "$mainAddress $membersAddresses" >>$NEW_VIRTUAL_ALIASES
    done

# Fetches Shared Mailboxes Main Addresses (groupOfNames)
# Fetches Shared Mailboxes Aliases (groupOfNames)

echo === Querying LDAP for Static Shared Mailboxes and their Aliases ===
echo ===

ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
	    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
	    -b "ou=groups,$OPENLDAP_BASE" \
	    -w "$OPENLDAP_BIND_PW" \
	    "(&(objectClass=groupOfNames)(mail=*)(objectClass=wsweetSharedMailbox))" cn 2>&1 \
    | awk '/^cn:/{print $2}' \
    | while read cn
    do
	mainAddress=$(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
				    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
				    -b "ou=groups,$OPENLDAP_BASE" \
				    -w "$OPENLDAP_BIND_PW" \
				    "(cn=$cn)" mail 2>&1 \
			| awk 'BEG{take=0}{if ($1 == "mail:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
			| tr '\n' '|' \
			| sed -e 's,| ,,g' -e 's,|$,,' \
			| tr [A-Z] [a-z])
	if ! check_address_valid $mainAddress; then
	    continue
	fi
	check_domain $mainAddress
	echo "$mainAddress OK" >>$NEW_VIRTUAL_MAILBOXES
	echo "$mainAddress $mainAddress" >>$NEW_VIRTUAL_ALIASES
	echo "$mainAddress lmtp:$LMTP_HOST:$LMTP_PORT" >>$NEW_TRANSPORTS
	for aliasAddress in $(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
					    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
					    -b "ou=groups,$OPENLDAP_BASE" \
					    -w "$OPENLDAP_BIND_PW" \
					    "(cn=$cn)" gosaMailAlternateAddress 2>&1 \
				| awk 'BEG{take=0}{if ($1 == "gosaMailAlternateAddress:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
				| tr '\n' '|' \
				| sed -e 's,| ,,g' -e 's,|$,,' \
				| tr [A-Z] [a-z])
	do
	    if ! check_address_valid $aliasAddress; then
		continue
	    fi
	    check_domain $aliasAddress
	    echo "$aliasAddress $mainAddress" >>$NEW_VIRTUAL_ALIASES
	done
    done

# Fetches Shared Mailboxes Main Addresses (groupOfURLs)
# Fetches Shared Mailboxes Aliases (groupOfURLs)

echo === Querying LDAP for Dynamic Shared Mailboxes and their Aliases ===
echo ===

ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
	    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
	    -b "ou=groups,$OPENLDAP_BASE" \
	    -w "$OPENLDAP_BIND_PW" \
	    "(&(objectClass=groupOfURLs)(mail=*)(objectClass=wsweetSharedMailbox))" cn 2>&1 \
    | awk '/^cn:/{print $2}' \
    | while read cn
    do
	mainAddress=$(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
				    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
				    -b "ou=groups,$OPENLDAP_BASE" \
				    -w "$OPENLDAP_BIND_PW" \
				    "(cn=$cn)" mail 2>&1 \
			| awk 'BEG{take=0}{if ($1 == "mail:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
			| tr '\n' '|' \
			| sed -e 's,| ,,g' -e 's,|$,,' \
			| tr [A-Z] [a-z])
	if ! check_address_valid $mainAddress; then
	    continue
	fi
	check_domain $mainAddress
	echo "$mainAddress OK" >>$NEW_VIRTUAL_MAILBOXES
	echo "$mainAddress $mainAddress" >>$NEW_VIRTUAL_ALIASES
	echo "$mainAddress lmtp:$LMTP_HOST:$LMTP_PORT" >>$NEW_TRANSPORTS
	for aliasAddress in $(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
					    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
					    -b "ou=groups,$OPENLDAP_BASE" \
					    -w "$OPENLDAP_BIND_PW" \
					    "(cn=$cn)" gosaMailAlternateAddress 2>&1 \
				| awk 'BEG{take=0}{if ($1 == "gosaMailAlternateAddress:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
				| tr '\n' '|' \
				| sed -e 's,| ,,g' -e 's,|$,,' \
				| tr [A-Z] [a-z])
	do
	    if ! check_address_valid $aliasAddress; then
		continue
	    fi
	    check_domain $aliasAddress
	    echo "$aliasAddress $mainAddress" >>$NEW_VIRTUAL_ALIASES
	done
    done

while :
do
    if test -s /etc/postfix/main.cf; then
	echo postfix configuration ready
	break
    fi
    echo waiting for postfix main configuration ...
    sleep 10
done

echo === Refreshinng Postfix Maps when necessary ===
echo ===

if test -s $NEW_TRANSPORTS; then
    if test "$MAILDROP_RELAY"; then
	MAILDROP_SUBDOMAIN=${MAILDROP_SUBDOMAIN:-maildrop}
	MAILDROP_PORT=${MAILDROP_PORT:-1025}
	echo "$MAILDROP_SUBOMAIN.$OPENLDAP_DOMAIN smtp:$MAILDROP_RELAY:$MAILDROP_PORT" \
	    >>$NEW_TRANSPORTS
    fi
    if test "$MAILHOG_RELAY"; then
	MAILHOG_SUBDOMAIN=${MAILHOG_SUBDOMAIN:-mailhog}
	MAILHOG_PORT=${MAILHOG_PORT:-1025}
	echo "$MAILHOG_SUBOMAIN.$OPENLDAP_DOMAIN smtp:$MAILHOG_RELAY:$MAILHOG_PORT" \
	    >>$NEW_TRANSPORTS
    fi
    if test "$SYMPA_RELAY"; then
	SYMPA_SUBDOMAIN=${SYMPA_SUBDOMAIN:-lists}
	if test "$SYMPA_PROTO" = smtps; then
	    RELAY_WITH=relay-smtps
	    SYMPA_PORT=${SYMPA_PORT:-465}
	else
	    RELAY_WITH=smtp
	    SYMPA_PORT=${SYMPA_PORT:-25}
	fi
	echo "$SYMPA_SUBDOMAIN.$OPENLDAP_DOMAIN $RELAY_WITH:$SYMPA_RELAY:$SYMPA_PORT" \
	    >>$NEW_TRANSPORTS
    fi
    if test "$SMTP_RELAY"; then
	if test "$SMTP_RELAY_PROTO" = smtps; then
	    RELAY_WITH=relay-smtps
	    SMTP_RELAY_PORT=${SMTP_RELAY_PORT:-465}
	else
	    RELAY_WITH=smtp
	    SMTP_RELAY_PORT=${SMTP_RELAY_PORT:-25}
	fi
	SMTP_RELAY_PORT=${SMTP_RELAY_PORT:-25}
	echo "* $RELAY_WITH:$SMTP_RELAY:$SMTP_RELAY_PORT" \
	    >>$NEW_TRANSPORTS
    fi
    if ! cmp $NEW_TRANSPORTS /etc/postfix/transport \
	    >/dev/null 2>&1; then
	cp $NEW_TRANSPORTS /etc/postfix/transport
	postmap /etc/postfix/transport
    fi
elif ! test -f /etc/postfix/transport; then
    touch /etc/postfix/transport
    postmap /etc/postfix/transport
fi
if test -s $NEW_VIRTUAL_ALIASES; then
    if ! cmp $NEW_VIRTUAL_ALIASES /etc/postfix/virtual_alias \
	    >/dev/null 2>&1; then
	cp $NEW_VIRTUAL_ALIASES /etc/postfix/virtual_alias
	postmap /etc/postfix/virtual_alias
    fi
elif ! test -f /etc/postfix/virtual_alias; then
    touch /etc/postfix/virtual_alias
    postmap /etc/postfix/virtual_alias
fi
if test -s $NEW_VIRTUAL_DOMAINS; then
    if ! cmp $NEW_VIRTUAL_DOMAINS /etc/postfix/virtual_domains \
	    >/dev/null 2>&1; then
	cp $NEW_VIRTUAL_DOMAINS /etc/postfix/virtual_domains
	postmap /etc/postfix/virtual_domains
    fi
elif ! test -f /etc/postfix/virtual_domains; then
    touch /etc/postfix/virtual_domains
    postmap /etc/postfix/virtual_domains
fi
if test -s $NEW_VIRTUAL_MAILBOXES; then
    if ! cmp $NEW_VIRTUAL_MAILBOXES /etc/postfix/virtual_mailbox \
	    >/dev/null 2>&1; then
	cp $NEW_VIRTUAL_MAILBOXES /etc/postfix/virtual_mailbox
	postmap /etc/postfix/virtual_mailbox
    fi
elif ! test -f /etc/postfix/virtual_mailbox; then
    touch /etc/postfix/virtual_mailbox
    postmap /etc/postfix/virtual_mailbox
fi

echo === Done Refreshinng Postfix configuration ===
echo ===

unset NEW_TRANSPORTS NEW_VIRTUAL_ALIASES NEW_VIRTUAL_DOMAINS \
    NEW_VIRTUAL_MAILBOXES
