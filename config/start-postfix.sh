#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

. /usr/local/bin/reset-tls.sh

if ! ls /etc/postfix/* >/dev/null 2>&1; then
    cp -rv /etc/postfix-ref/* /etc/postfix/
fi

mkdir -p /etc/postfix/ssl /var/log/postfix /etc/postfix/sasl
HAS_TLS=false
if test -s /etc/postfix/ssl/server.crt -a \
	-s /etc/postfix/ssl/server.key -a \
	"$RESET_SSL" = false; then
    echo Skipping Postfix SSL configuration - already initialized
    HAS_TLS=true
elif test -s /certs/tls.key -a -s /certs/tls.crt; then
    echo Initializing Postfix SSL configuration
    if ! test -s /certs/ca.crt; then
	if ! test -s /run/secrets/kubernetes.io/serviceaccount/ca.crt; then
	    cat <<EOT >&2
WARNING: Looks like there is no CA chain defined!
	 assuming it is not required or otherwise included in server
	 certificate definition
EOT
	    rm -f /etc/postfix/ssl/ca.crt
	    touch /etc/postfix/ssl/ca.crt
	else
	    cat /run/secrets/kubernetes.io/serviceaccount/ca.crt \
		>/etc/postfix/ssl/ca.crt
	fi
    else
	cat /certs/ca.crt >/etc/postfix/ssl/ca.crt
    fi
    cat /certs/tls.crt >/etc/postfix/ssl/server.crt
    cat /certs/tls.key >/etc/postfix/ssl/server.key
    chmod 0640 /etc/postfix/ssl/server.key
    HAS_TLS=true
fi

MYDOMAIN=${MYDOMAIN:-demo.local}
if test -z "$MYHOSTNAME"; then
    MYHOSTNAME=smtp.$MYDOMAIN
fi
if test -z "$PBLS"; then
    PBLS="pbl.spamhaus.org bl.spamcop.net opm.blitzed.org rbl.mail-abuse.org spamsources.fabel.dk zen.spamhaus.org zombie.dnsbl.sorbs.net"
fi
PBLS=$(echo "$PBLS" | sed 's| |, reject_rbl_client |g')

if test -z "$DONT_GENERATE_CONF"; then
    if test -s /etc/postfix/master.cf; then
	mv /etc/postfix/master.cf /etc/postfix/master.cf.old
    fi
    cat /master.cf >/etc/postfix/master.cf
    if test "$DO_SPF"; then
	cat <<EOF >>/etc/postfix/master.cf

spfcheck  unix  -       n       n       -       0       spawn
    user=policyd-spf argv=/usr/sbin/postfix-policyd-spf-perl
EOF
    fi
#    if ! $HAS_TLS; then
#	sed -i "/smtps/d" /etc/postfix/master.cf
#    fi
    if test -s /etc/postfix/main.cf; then
	mv /etc/postfix/main.cf /etc/postfix/main.cf.old
    fi
    LMTP_HOST=${LMTP_HOST:-127.0.0.1}
    LMTP_PORT=${LMTP_PORT:-2400}
    MAXSIZE=${MAXSIZE:-0}
    if test -z "$MYNETWORKS"; then
	MYNETWORKS=127.0.0.1/32
    fi
    sed -e "s|SMTP_FQDN|$MYHOSTNAME|" \
	-e "s|SMTP_MAXSIZE|$MAXSIZE|" \
	-e "s|SMTP_MYNETWORKS|$MYNETWORKS|" \
	-e "s|SMTP_PBLS|$PBLS|" \
	-e "s|SMTP_LMTP_HOST|$LMTP_HOST|" \
	-e "s|SMTP_LMTP_PORT|$LMTP_PORT|" \
	/main.cf >/etc/postfix/main.cf

    if test -z "$DO_SPF"; then
	sed -i "/check_policy_service .*spfcheck/d" /etc/postfix/main.cf
    else
	echo policyd-spf_time_limit = 3600 >>/etc/postfix/main.cf
    fi
    if ! $HAS_TLS; then
	sed -i "/smtp.*tls/d" /etc/postfix/main.cf
    fi

    if test -z "$DONT_GENERATE_MAPS"; then
	. /usr/local/bin/makemaps.sh
    fi
fi
if ! test -s /etc/aliases; then
    ROOT_ACCOUNT=${ROOT_ACCOUNT:-admin0}
    cat <<EOF >/etc/aliases
mailer-daemon: postmaster
postmaster: root
nobody: root
hostmaster: root
usenet: root
news: root
webmaster: root
www: webmaster
ftp: root
abuse: root
noc: root
security: root
postfix: root
root: $ROOT_ACCOUUNT@$MYDOMAIN
EOF
fi
postalias /etc/aliases

DKIM_HOST=${DKIM_HOST:-}
DKIM_PORT=${DKIM_PORT:-12345}
SPAMD_HOST=${SPAMD_HOST:-}
SPAMD_PORT=${SPAMD_PORT:-1783}
if test "$SPAMD_HOST" -a "$DKIM_HOST"; then
    MILTERS="inet:$SPAMD_HOST:$SPAMD_PORT inet:$DKIM_HOST:$DKIM_PORT"
elif test "$SPAMD_HOST"; then
    MILTERS="inet:$SPAMD_HOST:$SPAMD_PORT"
elif test "$DKIM_HOST"; then
    MILTERS="inet:$DKIM_HOST:$DKIM_PORT"
fi
for f in etc/ssl/certs/ca-certificates.crt etc/services etc/resolv.conf \
    etc/hosts etc/localtime etc/nsswitch.conf etc/host.conf
do
    b=`dirname $f`
    if ! test -d /var/spool/postfix/$b; then
	mkdir -p /var/spool/postfix/$b
    fi
    cat /$f >/var/spool/postfix/$f
done
if ! test -s /etc/postfix/sasl/smtpd.conf; then
    cat <<EOF >/etc/postfix/sasl/smtpd.conf
pwcheck_method: saslauthd
auxprop_plugin: ldapdb
mech_list: PLAIN LOGIN
allowplaintext: yes
EOF
fi
cat /etc/postfix/sasl/smtpd.conf >/etc/postfix/smtpd.conf
cat /etc/postfix/sasl/smtpd.conf >/usr/lib/sasl2/smtpd.conf
if test -L /etc/postfix/makedefs.out; then
    rm -f /etc/postfix/makedefs.out
fi
if ! test -s /etc/postfix/makedefs.out; then
    cat /usr/share/postfix/makedefs.out >/etc/postfix/makedefs.out
fi
if test "$MILTERS"; then
    if grep ^smtpd_milters /etc/postfix/main.cf >/dev/null; then
	sed -i "s|^smtpd_milters.*|smtpd_milters = $MILTERS|" \
	    /etc/postfix/main.cf
    else
	echo "smtpd_milters = $MILTERS" >>/etc/postfix/main.cf
    fi
    if grep ^non_smtpd_milters /etc/postfix/main.cf >/dev/null; then
	sed -i "s|^non_smtpd_milters.*|non_smtpd_milters = $MILTERS|" \
	    /etc/postfix/main.cf
    else
	echo "non_smtpd_milters = $MILTERS" >>/etc/postfix/main.cf
    fi
    if grep ^milter_default_action /etc/postfix/main.cf >/dev/null; then
	sed -i 's|^milter_default_action.*|milter_default_action = accept|' \
	    /etc/postfix/main.cf
    else
	echo milter_default_action = accept >>/etc/postfix/main.cf
    fi
    if grep ^milter_protocol /etc/postfix/main.cf >/dev/null; then
	sed -i 's|^milter_protocol.*|milter_protocol = 6|' \
	    /etc/postfix/main.cf
    else
	echo milter_protocol = 6 >>/etc/postfix/main.cf
    fi
else
    if grep ^smtpd_milters /etc/postfix/main.cf >/dev/null; then
	sed -i "/^smtpd_milters.*/d" /etc/postfix/main.cf
    fi
    if grep ^non_smtpd_milters /etc/postfix/main.cf >/dev/null; then
	sed -i "/^non_smtpd_milters.*/d" /etc/postfix/main.cf
    fi
    if grep ^milter_default_action /etc/postfix/main.cf >/dev/null; then
	sed -i '/^milter_default_action.*/d' /etc/postfix/main.cf
    fi
fi

while :
do
    if test -e /run/saslauthd/mux; then
	echo saslauthd ready
	break
    fi
    echo waiting for saslauthd ...
    sleep 10
done

if test "$DONT_GENERATE_MAPS"; then
    while :
    do
	if test -s /etc/postfix/virtual_mailbox; then
	    echo postfix maps ready
	    break
	fi
	echo waiting for postix maps ...
	sleep 10
    done
fi

unset OPENLDAP_BIND_DN_PREFIX OPENLDAP_BIND_PW OPENLDAP_DOMAIN PBLS LMTP_PORT \
    OPENLDAP_HOST OPENLDAP_PROTO OPENLDAP_USERS_OBJECTCLASS MYNETWORKS \
    MYNETWORKS MYHOSTNAME MYDOMAIN LMTP_HOST DKIM_HOST DKIM_PORT RESET_SSL \
    SPAMD_HOST SPAMD_PORT MILTERS

echo Starting Postfix
exec postfix start-fg
