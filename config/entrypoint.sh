#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

if test -z "$1"; then
    echo command not set
    if test "$DEBUG"; then
	sleep 86400
    fi
    exit 1
fi

ln -sf /run/rsyslog/dev/log /dev/log

exec $@
