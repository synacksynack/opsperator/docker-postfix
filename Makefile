SKIP_SQUASH?=1
IMAGE=opsperator/postfix
-include Makefile.cust

.PHONY: build
build:
	@@SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: genkey
genkey:
	@@mkdir -p dkim.d
	@@( cd dkim.d && opendkim-genkey -s default -d ci.apps.intra.unetresgrossebite.com )

.PHONY: kubekey
kubekey: genkey
	kubectl create secret generic -n ci postfix-kube-dkim \
	    --from-file=default.private=dkim.d/default.private \
	    --from-file=default.txt=dkim.d/default.txt

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in secret service config rbac statefulset deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "CYRUS_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "CYRUS_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: ocpurge
ocpurge: occheck
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
	@@rm -fr dkim.d

.PHONY: testldap
testldap:
	@@docker rm -f testldap || echo ok
	@@docker run --name testldap \
	    --tmpfs /var:rw,size=65536k \
	    --tmpfs /var/run:rw,size=65536k  \
	    --tmpfs /run/ldap:rw,size=65536k  \
	    --tmpfs /usr/local/openldap/etc/openldap:rw,size=65536k \
	    --tmpfs /var/lib/ldap:rw,size=65536k \
	    -e OPENLDAP_DEBUG=yesplease \
	    -e OPENLDAP_GLOBAL_ADMIN_PASSWORD=secret \
	    -e OPENLDAP_POSTFIX_PASSWORD=secret4242424242 \
	    -e DEBUG=yesplease \
	    -e DO_EXPORTERS=true \
	    -e DO_LEMON=true \
	    -e DO_NEXTCLOUD=true \
	    -e DO_POSTFIX=true \
	    -e DO_SSP=true \
	    -e DO_SERVICEDESK=true \
	    -e DO_WHITEPAGES=true \
	    -e OPENLDAP_BIND_LDAP_PORT=1389 \
	    -e OPENLDAP_BIND_LDAPS_PORT=1636 \
	    -e OPENLDAP_INIT_DEBUG_LEVEL=256 \
	    -p 1389:1389 -p 1636:1636 \
	    -d opsperator/openldap

.PHONY: test
test:
	@@mkdir -p volume/saslauthd volume/rsyslog volume/postfix || echo meh
	@@chmod -R 777 volume || echo warning
	@@docker rm -f testpostfixsmtp || echo ok
	@@docker rm -f testpostfixsyslog || echo ok
	@@docker rm -f testpostfixsaslauthd || echo ok
	@@docker rm -f testpostfixjob || echo ok
	@@docker run --name testpostfixsmtp \
	    --privileged \
	    -e DONT_GENERATE_MAPS=yay \
	    -e LMTP_HOST=127.0.0.1 \
	    -e LMTP_PORT=2400 \
	    -v `pwd`/volume/postfix:/etc/postfix \
	    -v `pwd`/volume/saslauthd:/run/saslauthd \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    --tmpfs /var/spool/postfix:rw,size=65536k  \
	    -v `pwd`/volume/saslauthd:/var/spool/postfix/var/run/saslauthd \
	    -d $(IMAGE) /start-postfix.sh
	@@docker run --name testpostfixsyslog \
	    --privileged \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    -d $(IMAGE) /start-syslog.sh
	@@ldapip=`docker inspect testldap | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	docker run --name testpostfixsaslauthd \
	    --privileged \
	    -e OPENLDAP_BIND_PW=secret4242424242 \
	    -e OPENLDAP_HOST=$$ldapip \
	    -e OPENLDAP_PROTO=ldap \
	    -e OPENLDAP_PORT=1389 \
	    -v `pwd`/volume/saslauthd:/run/saslauthd \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    -d $(IMAGE) /start-saslauthd.sh
	@@ldapip=`docker inspect testldap | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	docker run --name testpostfixjob \
	    --privileged \
	    -e LMTP_HOST=127.0.0.1 \
	    -e LMTP_PORT=2400 \
	    -e OPENLDAP_BIND_PW=secret4242424242 \
	    -e OPENLDAP_HOST=$$ldapip \
	    -e OPENLDAP_PROTO=ldap \
	    -e OPENLDAP_PORT=1389 \
	    -v `pwd`/volume/postfix:/etc/postfix \
	    -v `pwd`/volume/saslauthd:/run/saslauthd \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    -d $(IMAGE) /run-job.sh
