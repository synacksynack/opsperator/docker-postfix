# k8s Postfix

Postfix Base image for Kubernetes.

WARNING: privileged containers!

Depends on a Cyrus server, such as
https://gitlab.com/synacksynack/opsperator/docker-cyrus

Depends on an OpenLDAP server, such as
https://gitlab.com/synacksynack/opsperator/docker-openldap

Build with:

```
$ make build
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Test LDAP auth:

```
$ kubectl exec -n ci deploy/postfix-kube -- testsaslauthd -u admin0 -p test-admin-pw
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name               |    Description                     | Default                                                     |
| :----------------------------- | ---------------------------------- | ----------------------------------------------------------- |
|  `DKIM_HOST`                   | OpenDKIM Host Address              | undef (`127.0.0.1` when starting OpenDKIM itself)           |
|  `DKIM_PORT`                   | OpenDKIM Bind Port                 | `12345`                                                     |
|  `LMTP_HOST`                   | LMTP Host Address                  | `127.0.0.1`                                                 |
|  `LMTP_PORT`                   | LMTP Bind Port                     | `24`                                                        |
|  `MAILDROP_PORT`               | Maildrop SMTP Port                 | `1025`                                                      |
|  `MAILDROP_RELAY`              | Maildrop SMTP Relay                | undef                                                       |
|  `MAILDROP_SUBDOMAIN`          | Maildrop Subdomain                 | `maildrop`                                                  |
|  `MAILHOG_PORT`                | Mailhog SMTP Port                  | `1025`                                                      |
|  `MAILHOG_RELAY`               | Mailhog SMTP Relay                 | undef                                                       |
|  `MAILHOG_SUBDOMAIN`           | Mailhog Subdomain                  | `mailhog`                                                   |
|  `MAXSIZE`                     | Postfix Message Size Limit         | `0`                                                         |
|  `MYDOMAIN`                    | Postfix Domain Name                | `demo.local`                                                |
|  `MYHOSTNAME`                  | Postfix FQDN                       | `smtp.$MYDOMAIN`                                            |
|  `MYNETWORKS`                  | Postfix My Netwoks                 | `127.0.0.1/32`                                              |
|  `OPENLDAP_BASE`               | OpenLDAP Base                      | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` |
|  `OPENLDAP_BIND_DN_RREFIX`     | OpenLDAP Bind DN Prefix            | `cn=postfix,ou=services`                                    |
|  `OPENLDAP_BIND_PW`            | OpenLDAP Bind Password             | `secret`                                                    |
|  `OPENLDAP_DOMAIN`             | OpenLDAP Domain Name               | `demo.local`                                                |
|  `OPENLDAP_HOST`               | OpenLDAP Backend Address           | undef                                                       |
|  `OPENLDAP_PORT`               | OpenLDAP Bind Port                 | `389` or `636` depending on `OPENLDAP_PROTO`                |
|  `OPENLDAP_PROTO`              | OpenLDAP Proto                     | `ldap`                                                      |
|  `OPENLDAP_USERS_OBJECTCLASS`  | OpenLDAP Users ObjectClass         | `inetOrgPerson`                                             |
|  `PBLS`                        | Postfix Policy Block Lists         | `pbl.spamhaus.org bl.spamcop.net opm.blitzed.org`           |
|                                |                                    | `rbl.mail-abuse.org spamsources.fabel.dk zen.spamhaus.org`  |
|                                |                                    | `zombie.dnsbl.sorbs.net`                                    |
|  `ROOT_ACCOUNT`                | Alias receiving system mails       | `admin0`                                                    |
|  `SMTP_RELAY`                  | Outbound SMTP Relay                | undef                                                       |
|  `SMTP_RELAY_PORT`             | Outbound SMTP Port                 | `25`                                                        |
|  `SMTP_RELAY_PROTO`            | Outbound SMTP Proto                | `smtp`                                                      |
|  `SPAMD_HOST`                  | AntiSpam Milter Host Address       | undef                                                       |
|  `SPAMD_PORT`                  | AntiSpam Milter Port               | `1783`                                                      |
|  `SYMPA_PORT`                  | Sympa SMTP Port                    | `25`                                                        |
|  `SYMPA_PROTO`                 | Sympa SMTP Proto                   | `smtp`                                                      |
|  `SYMPA_RELAY`                 | Sympa SMTP Relay                   | undef                                                       |
|  `SYMPA_SUBDOMAIN`             | Sympa Subdomain                    | `lists`                                                     |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point                 | Description                                   |
| :---------------------------------- | --------------------------------------------- |
|  `/certs`                           | Postfix Certificates & Authorities (optional) |
|  `/etc/dkim.d/keys`                 | OpenDKIM Domains Private Keys                 |
|  `/etc/postfix`                     | Postfix Configuration Directories             |
|  `/var/spool/postfix`               | Postfix Queue                                 |
|  `/var/spool/postfix/run/saslauthd` | Postfix Rsyslog Socket (postfix chroot)       |
|  `/run/rsyslog`                     | Postfix Rsyslog Socket                        |
|  `/run/saslauthd`                   | Postfix Saslauthd Socket                      |
|  `/var/log/postfix`                 | Rsyslog `mail.*` logs (for Prometheus)        |
