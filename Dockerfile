FROM docker.io/debian:buster-slim

# Postfix image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive \
    SMTPCHKURL=https://github.com/nabeken/go-check-smtp/releases/download \
    SMTPCHKVERS=v2020051904

LABEL io.k8s.description="Postfix SMTP server." \
      io.k8s.display-name="Postfix" \
      io.openshift.expose-services="25:smtp,465:smtps,587:submission" \
      io.openshift.tags="postfix,opendkim,policyd-spf" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-postfix" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="3.4.14"

COPY config/* /

RUN set -x \
    && echo "# Install Dumb-init" \
    && apt-get update \
    && apt-get -y install dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Postfix" \
    && apt-get install --no-install-recommends -y rsyslog postfix sasl2-bin \
	postfix-policyd-spf-perl opendkim ca-certificates ldap-utils wget \
	libsasl2-modules opendmarc \
    && mv /docker-postfix.conf /etc/rsyslog.d/ \
    && mv /log_rotate.sh /check_smtp.sh /makemaps.sh /reset-tls.sh \
	/usr/local/bin/ \
    && mv /etc/postfix /etc/postfix-ref \
    && mkdir /etc/postfix /var/log/postfix \
    && if uname -m | grep x86_64 >/dev/null; then \
	echo "# Install SMTP Check" \
	&& wget -O /usr/src/check_smtp.tar.gz \
	    "$SMTPCHKURL/$SMTPCHKVERS/go-check-smtp_linux_amd64.tar.gz" \
	&& tar --strip-components=1 -C /usr/src -xzf /usr/src/check_smtp.tar.gz \
	&& mv /usr/src/go-check-smtp /usr/bin/check_smtp \
	&& chown root:root /usr/bin/check_smtp \
	&& chmod 555 /usr/bin/check_smtp \
	&& rm -fr /usr/src/*; \
    fi \
    && echo "# Cleaning Up" \
    && apt-get -y remove --purge gnupg2 wget \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rvf /usr/share/man /usr/share/doc /var/lib/apt/lists/* \
	/etc/postfix-ref/main.cf /etc/postfix-ref/master.cf /etc/alias* \
	/etc/postfix/ssl \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

EXPOSE 25 465 587
ENTRYPOINT ["dumb-init","--","/entrypoint.sh"]
